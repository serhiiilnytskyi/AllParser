import Enums.SelectorType;
import org.apache.log4j.Logger;

public class Selector {

    private static final Logger LOG = Logger.getLogger(Selector.class);

    private String name;
    private SelectorType selectorType;
    private String selectorquery;
    private boolean needClick;
    private String attributeToTake;
    private Integer waitTime = 10;

    public Integer getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SelectorType getSelectorType() {
        return selectorType;
    }

    public void setSelectorType(SelectorType selectorType) {
        this.selectorType = selectorType;
    }

    public String getSelectorquery() {
        return selectorquery;
    }

    public void setSelectorquery(String selectorquery) {
        this.selectorquery = selectorquery;
    }


    public boolean isNeedClick() {
        return needClick;
    }

    public void setNeedClick(boolean needClick) {
        this.needClick = needClick;
    }

    public String getAttributeToTake() {
        return attributeToTake;
    }

    public void setAttributeToTake(String attributeToTake) {
        this.attributeToTake = attributeToTake;
    }

    public Selector(String name, SelectorType selectorType, String selectorquery) {
        this.name = name;
        this.selectorType = selectorType;
        this.selectorquery = selectorquery;
        this.needClick = false;
    }


}
