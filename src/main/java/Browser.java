import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Browser  {
    private static final Logger log = Logger.getLogger(Browser.class);

    private WebDriver driver;

    private static WebDriverWait waitTimeout;

    private boolean isBrowserBusy = true;

    private static JavascriptExecutor jsExec;


    public WebDriver getDriver() {
        return driver;
    }

    public static WebDriverWait getWaitTimeout() {
        return waitTimeout;
    }

    public static void setWaitTimeout(WebDriverWait waitTimeout) {
        Browser.waitTimeout = waitTimeout;
    }

    public boolean isBrowserBusy() {
        return isBrowserBusy;
    }

    public void setBrowserBusy(boolean browserBusy) {
        isBrowserBusy = browserBusy;
    }

    public Browser(){
        openWebDriver();
    }

    public void openWebDriver() {
        isBrowserBusy = true;

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        this.driver = new ChromeDriver();
        setWaitTimeout(new WebDriverWait( driver, 3 ));
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

        log.info("Driver created");
        isBrowserBusy = false;
    }

    public void close(){
        driver.close();
        driver.quit();
        isBrowserBusy = true;
    }

    public void openWebPage(String url){
        isBrowserBusy = true;
        this.driver.get(url);

        log.info("Page opened: " +  url);
        isBrowserBusy = false;
    }

    public void parse(String url) {
        openWebDriver();
        openWebPage(url);
        waitJQueryAngular();


    }

    public String getFullPageSource(){
        return driver.getPageSource();
    }

    public List<WebElement> findElement(Selector selector){
        isBrowserBusy = true;

        WebElement webElement = null;
        List<WebElement> webElements = new ArrayList<WebElement>();

        try {
            switch (selector.getSelectorType()) {
                case TAGNAME:
                    log.info("Use Tag Name search");
                    webElement = waitTimeout.until(ExpectedConditions.visibilityOfElementLocated(new By.ByTagName(selector.getSelectorquery())));
                    break;
                case ID:
                    log.info("Use search by ID");
                    webElement = waitTimeout.until(ExpectedConditions.visibilityOfElementLocated(new By.ById(selector.getSelectorquery())));
                    break;
                case XPATH:
                    log.info("Use search by xPath");
                    webElements =  (new WebDriverWait(driver, selector.getWaitTime() )).until(ExpectedConditions.presenceOfAllElementsLocatedBy(new By.ByXPath(selector.getSelectorquery())));
                   // webElement = (new WebDriverWait(driver, 30 )).until(ExpectedConditions.presenceOfElementLocated(By.xpath(selector.getSelectorquery())));
                    break;
                case CSSSELECTOR:
                    log.info("Use search by CssSelector");
                    webElement =  waitTimeout.until(ExpectedConditions.visibilityOfElementLocated(new By.ByCssSelector(selector.getSelectorquery())));
                    break;
                default:
                    break;

                // must write another methods
            }
            if (selector.isNeedClick()) {
                try {
                    for (WebElement element: webElements) {

                        element.click();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //TODO work with exception
                }
            }


        } catch (Exception e) {
            log.debug("Element not found, timeout or nothing on ");
        }
        if (webElements != null){
            log.info("Found " + webElements.size() + " WebElements on page");
        }else {
            log.info("Nothing to find -- query is bad");
        }
        isBrowserBusy = false;

        return webElements;
    }

    //Wait for JQuery Load
    public static void waitForJQueryLoad() {
        //Wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = driver -> ((Long) ((JavascriptExecutor) driver)
                .executeScript("return jQuery.active") == 0);

        //Get JQuery is Ready
        boolean jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");

        //Wait JQuery until it is Ready!
        if(!jqueryReady) {
            System.out.println("JQuery is NOT Ready!");
            //Wait for jQuery to load
            waitTimeout.until(jQueryLoad);
        } else {
            System.out.println("JQuery is Ready!");
        }
    }


    //Wait for Angular Load
    public  void waitForAngularLoad() {
        WebDriverWait wait = new WebDriverWait( driver,15);
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;

        String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";

        //Wait for ANGULAR to load
        ExpectedCondition<Boolean> angularLoad = driver -> Boolean.valueOf(((JavascriptExecutor) driver)
                .executeScript(angularReadyScript).toString());

        //Get Angular is Ready
        boolean angularReady = Boolean.valueOf(jsExec.executeScript(angularReadyScript).toString());

        //Wait ANGULAR until it is Ready!
        if(!angularReady) {
            System.out.println("ANGULAR is NOT Ready!");
            //Wait for Angular to load
            wait.until(angularLoad);
        } else {
            System.out.println("ANGULAR is Ready!");
        }
    }

    //Wait Until JS Ready
    public  void waitUntilJSReady() {
        WebDriverWait wait = new WebDriverWait(driver,15);
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;

        //Wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = driver -> ((JavascriptExecutor) driver)
                .executeScript("return document.readyState").toString().equals("complete");

        //Get JS is Ready
        boolean jsReady =  (Boolean) jsExec.executeScript("return document.readyState").toString().equals("complete");

        //Wait Javascript until it is Ready!
        if(!jsReady) {
            System.out.println("JS in NOT Ready!");
            //Wait for Javascript to load
            wait.until(jsLoad);
        } else {
            System.out.println("JS is Ready!");
        }
    }

    //Wait Until JQuery and JS Ready
    public  void waitUntilJQueryReady() {
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;

        //First check that JQuery is defined on the page. If it is, then wait AJAX
        Boolean jQueryDefined = (Boolean) jsExec.executeScript("return typeof jQuery != 'undefined'");
        if (jQueryDefined == true) {
            //Pre Wait for stability (Optional)
            sleep(20);

            //Wait JQuery Load
            waitForJQueryLoad();

            //Wait JS Load
            waitUntilJSReady();

            //Post Wait for stability (Optional)
            sleep(20);
        }  else {
            System.out.println("jQuery is not defined on this site!");
        }
    }

    //Wait Until Angular and JS Ready
    public void waitUntilAngularReady() {
        JavascriptExecutor jsExec = (JavascriptExecutor) driver;

        //First check that ANGULAR is defined on the page. If it is, then wait ANGULAR
        Boolean angularUnDefined = (Boolean) jsExec.executeScript("return window.angular === undefined");
        if (!angularUnDefined) {
            Boolean angularInjectorUnDefined = (Boolean) jsExec.executeScript("return angular.element(document).injector() === undefined");
            if(!angularInjectorUnDefined) {
                //Pre Wait for stability (Optional)
                sleep(20);

                //Wait Angular Load
                waitForAngularLoad();

                //Wait JS Load
                waitUntilJSReady();

                //Post Wait for stability (Optional)
                sleep(20);
            } else {
                System.out.println("Angular injector is not defined on this site!");
            }
        }  else {
            System.out.println("Angular is not defined on this site!");
        }
    }

    //Wait Until JQuery Angular and JS is ready
    public void waitJQueryAngular() {
        waitUntilJQueryReady();
        waitUntilAngularReady();
    }

    public static void sleep (Integer seconds) {
        long secondsLong = (long) seconds;
        try {
            Thread.sleep(secondsLong);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
