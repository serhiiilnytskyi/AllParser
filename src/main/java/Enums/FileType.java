package Enums;

public enum FileType {
    CSV,
    XML,
    XLS
}
