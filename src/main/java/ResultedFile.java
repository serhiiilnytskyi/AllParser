import Enums.FileType;

import java.io.*;

public class ResultedFile {

    private File file;
    private FileType fileType;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public ResultedFile(String filePath, FileType fileType) {
        this.file = new File(filePath);
        this.fileType = fileType;
    }

    public boolean writeToFile(String resultedSt){
        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw))
        {
            out.println(resultedSt);
            return true;
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
            return false;
        }

    }


}
