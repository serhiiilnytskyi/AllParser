import java.util.Arrays;
import java.util.Queue;

import org.apache.log4j.Logger;

import java.util.regex.Pattern;

public class UrlGenerationTask extends UrlTask {

    private static final Logger LOG = Logger.getLogger(UrlGenerationTask.class);

    //Todo create functionality with regex baseUrl
    private String baseUrl;

    private int beginIndex;

    private int endIndex;

    private int step;

    private String[] parts;



    public UrlGenerationTask(String baseUrl) {

        this.baseUrl = baseUrl;

        Pattern pattern = Pattern.compile("\\[|\\]");
        parts = pattern.split(baseUrl);

        if (parts.length > 1) {
            Pattern indexPattern = Pattern.compile(",");
            String[] indexes = indexPattern.split(parts[1]);

            this.beginIndex = Integer.parseInt(indexes[0]);
            this.endIndex = Integer.parseInt(indexes[1]);

            if (2 == indexes.length) {
                this.step = 1;
            } else {
                this.step = Integer.parseInt(indexes[2]);
            }
        }

        LOG.info("UrlGeneration Task Created. Base url: " + baseUrl + " .");

    }

    /** {@inheritDoc} */
    @Override
    public void run() {
        LOG.info("UrlGeneration Task started.");
        for (int i = beginIndex; i <= endIndex; i=i + step) {
            String url = parts[0] + i;
            if(parts.length == 3){
                url += parts[2];
            }
            addUrl(url);
        }

        LOG.info("UrlGeneration Task ended. Results: " + getUrlsPagesQueue().size() + " .");

    }

}
