import Enums.FileType;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.*;

public class TaskParsing extends Task {

    private static final Logger LOG = Logger.getLogger(TaskParsing.class);

    private static final String SEPARATOR = ";";

    private List<Selector> selectors;

    private Browser browser;

    private List<Map<String, String>> results = new ArrayList<Map<String, String>>();

    private String url;

    private boolean isWorking;

    ResultedFile resultedFile = new ResultedFile("result2", FileType.CSV);

    public List<Selector> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<Selector> selectors) {
        this.selectors = selectors;
    }

    public Browser getBrowser() {
        return browser;
    }

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }

    public List<Map<String, String>> getResults() {
        return results;
    }

    public void setResults(List<Map<String, String>> results) {
        this.results = results;
    }

    public TaskParsing(List<Selector> selectors) {
        //Create new browser for search
        browser = new Browser();
        this.selectors = selectors;
        isWorking = false;
    }

    public TaskParsing(Selector selector) {
        //Create new browser for search
        browser = new Browser();
        this.selectors = new ArrayList<Selector>();
        selectors.add(selector);
        isWorking = false;
    }

    public void nextPage(String url) {
        browser.openWebPage(url);
        results.add(findAllElements());
    }

    public Map<String, String> findAllElements() {


        Map<String, String> results = new HashMap<String, String>();
        StringBuffer resultedSt = new StringBuffer("");

        for (Selector selector : selectors) {


            List<WebElement> elements = new ArrayList<WebElement>();
            elements.addAll(browser.findElement(selector));

            String name = selector.getName();

            int i = 0;

            for (WebElement element : elements) {


                try {
                    results.put(name + i++, element.getText());
                    LOG.info("Putted result Key: " + name + "        Text: " + element.getText());
                    resultedSt.append(element.getText() + " ");
                } catch (Exception e) {
                    LOG.error("Error with finding: " + name);
                }

            }
            resultedSt.append(SEPARATOR);
        }



        resultedFile.writeToFile(resultedSt.toString().replaceAll("\r|\n", " "));


        return results;
    }


    @Override
    public void run() {
        while(Task.getUrlsQueue().size() != 0) {
            nextPage(Task.pollUrl()/*Task.getUrlsQueue().remove()*/);
        }
    }
}
