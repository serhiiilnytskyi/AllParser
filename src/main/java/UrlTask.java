import java.lang.reflect.Array;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class UrlTask extends Task {

    private static Queue<String> urlsPagesQueue = new ConcurrentLinkedQueue<String>();

    /**
     * Getter for property 'urlsPagesQueue'.
     *
     * @return Value for property 'urlsPagesQueue'.
     */
    public static Queue<String> getUrlsPagesQueue() {
        return urlsPagesQueue;
    }

    /**
     * Setter for property 'urlsPagesQueue'.
     *
     * @param urlsPagesQueue Value to set for property 'urlsPagesQueue'.
     */
    public static void setUrlsPagesQueue(Queue<String> urlsPagesQueue) {
        UrlTask.urlsPagesQueue = urlsPagesQueue;
    }


    public static boolean addUrl(String url){
        return urlsPagesQueue.offer(url);
    }

    public static String pollUrl(){
        return urlsPagesQueue.poll();
    }
}
