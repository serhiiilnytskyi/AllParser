import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UrlParsingTask extends UrlTask {


    private static final Logger LOG = Logger.getLogger(UrlParsingTask.class);

    private List<Selector> selectors;
    private Browser browser;


    private List<String> results = new ArrayList<String>();

    public UrlParsingTask(Selector selector) {
        selectors = new ArrayList<Selector>();
        selectors.add(selector);
    }

    public UrlParsingTask(List<Selector> selectors) {
        this.selectors = selectors;
        browser = new Browser();
    }

    public void findAllElements() {

        for (Selector selector : selectors) {

            List<WebElement> elements = new ArrayList<WebElement>();
            elements.addAll(browser.findElement(selector));

            for (WebElement element : elements) {
                results.add(element.getAttribute(selector.getAttributeToTake()));
                Task.addUrl(element.getAttribute(selector.getAttributeToTake()));
                LOG.info("Resulted url: " + element.getAttribute(selector.getAttributeToTake()));

            }
        }
    }

    public void nextPage(String url) {
        browser.openWebPage(url);
    }

    @Override
    public void run() {
        while (!UrlTask.getUrlsPagesQueue().isEmpty()) {
            String url = UrlTask.pollUrl();
            nextPage(url);
            findAllElements();
            //Todo adding many urls and method to change types to String
            //todo another way to work Thread ParsingURL to add URLS to queue permanently
            }
        }

}
