

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public abstract class Task implements Runnable{

    private static Queue<String> urlsQueue = new LinkedBlockingQueue<String>();

    /**
     * Getter for property 'urlsQueue'.
     *
     * @return Value for property 'urlsQueue'.
     */
    public static Queue<String> getUrlsQueue() {
        return urlsQueue;
    }

    /**
     * Setter for property 'urlsQueue'.
     *
     * @param urlsQueue Value to set for property 'urlsQueue'.
     */
    public static void setUrlsQueue(Queue<String> urlsQueue) {
        Task.urlsQueue = urlsQueue;
    }

    public static boolean addUrl(String url){
        return urlsQueue.offer(url);
    }

    public static String pollUrl(){
        String s  = urlsQueue.poll();

        return s;
    }

}
